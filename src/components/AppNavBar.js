import React, {useContext} from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	const {user} = useContext(UserContext);
	console.log(user)

	return(
		
		<Navbar expand="lg">
		  <Container fluid className="nav-container">
		    <Navbar.Brand as={Link} to="/" className="nav-brand">AnimeTaku</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto my-lg-0">
		        <Nav.Link as={Link} to="/">Home</Nav.Link>
		        <Nav.Link as={Link} to="/products">Products</Nav.Link>
		     
		      </Nav>
		      <Nav className="d-flex">
		      	{
		    		(user.id === null) ?
			       	<>
				       	<Nav.Link className="navlink" as={Link} to="/register">Register</Nav.Link>
				        <Nav.Link className="navlink" as={Link} to="/login">Log In</Nav.Link>
			        </>
		    	:	
		        	
		        	(user.id !== null && !user.isAdmin) ?
				       	<>
					       	<Nav.Link className="navlink" as={Link} to="/cart">Cart</Nav.Link>
					       	<Nav.Link className="navlink" as={Link} to="/logout">Logout</Nav.Link>
				       	</>
			       	: 
				       	<>
				       		<Nav.Link className="navlink" as={Link} to="/admin">Admin Dashboard</Nav.Link>
				       		<Nav.Link className="navlink" as={Link} to="/logout">Logout</Nav.Link>
				       	</>
		    	}
		    	
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	)
}
