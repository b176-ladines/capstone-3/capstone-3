import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){

	console.log(productProp);

	console.log(typeof productProp);

	const {name, description, price, _id} = productProp

	return(
		<Card className="col-12 col-lg-3 p-3">
			<Card.Title>
				<h4>{name}</h4>
			</Card.Title>
			<Card.Body>
				<h6>Description:</h6>
				<p>{description}</p>
				<h6>Price:</h6>
				<p>Php {price}</p>

				<Button className="btn-prod" as= {Link} to={`/products/${_id}`}>See Details</Button>
			</Card.Body>
		</Card>
	)
};
