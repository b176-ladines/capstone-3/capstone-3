import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const {productId} = useParams();

	const buy = (productId) => {
		fetch('http://localhost:4000/users/buy', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				name : name,
				description : description,
				price : price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){

				Swal.fire({
					title: "Added to Cart!",
					icon: "success",
					text: "You have successfully added to cart"
				})

				history("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

return(
	<Container className="mt-5">
		<Row>
			<Col className="offset-4">
				<Card className="p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>

						<Card.Subtitle>Availability</Card.Subtitle>
						<Card.Text>In Stock</Card.Text>
						
						{ user.id !== null ?
							<Button className="btn-prod" onClick={() => buy(productId)}>Add to Cart</Button>
							:
							<Link className="btn btn-danger" to="/login">Log In to Buy</Link>
						}

					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}
