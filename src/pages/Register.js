import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import registerImg from '../images/register.jpg'



export default function Register(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		if(data === true){
			Swal.fire({
				title: "Email Already Exists",
				icon: "error",
				text: "Please provide another email"
			})
		} else {
			fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data !== false){
					console.log(data)
					Swal.fire({
						title: 'Registration successful',
						icon: 'success',
						text: 'Welcome to String Files!'
					})
					setFirstName('');
					setLastName('');
					setMobileNo('');
					setEmail('');
					setPassword('');
					history("/login")

				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again'
					})
				}
				})
			}

		})
		
	}

	useEffect(() => {

		if(firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password !== ''){
			setIsActive(true);

		} else {
			setIsActive(false);
		}

	}, [firstName, lastName, mobileNo, email, password])

	return(

		(user.id !== null) ?
			<Navigate to= "/"/>
		:
		<>
		<div className="circle2"></div>
		<Row className="justify-content-center registerForm">
		<Col className="col-12 col-md-5 order-2">
		<Form onSubmit={e => registerUser(e)}>
			<h1 className="text-white">Register</h1>
			
			<Form.Group controlId="firstName">
				<Form.Label className="text-white">First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your First Name here"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label className="text-white">Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your Last Name here"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

				<Form.Group controlId="mobileNo">
				<Form.Label className="text-white">Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your Mobile Number here"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label className="text-white">Email Address</Form.Label>
				<Form.Control
					type= "email"
					placeholder= "Enter your email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label className="text-white">Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
						
			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
				Submit
				</Button>
			}


		</Form>
		</Col>
		<Col className="col-12 col-md-6 order-1">
			<img src={registerImg} className='img-fluid shadow-4' alt='...' />
		</Col>
		</Row>
		</>

	)
}
