import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import loginImg from '../images/login.png'

export default function Login(props){

	const {user, setUser} = useContext(UserContext);
	console.log(user)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function loginUser(e){
		e.preventDefault()


		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' :  'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to String Files!'
				})
			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})

		setEmail('')
		setPassword('')

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}
	}


	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.id !== null) ?
		
		<Navigate to= "/"/>
		
		:
		<>
		<div className="circle2"></div>
		<Row className="loginForm">
		<Col className="col-12 col-md-6 order-2">
		<Form onSubmit={e => loginUser(e)} className="text-white pt-5 mx-auto">
		<h1>Log In</h1>
			<Form.Group controlId="email">
				<Form.Label className="text-white">Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label className="text-white">Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
					Log In
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
					Log In
				</Button>
			}
		</Form>
		</Col>
		<Col className="col-12 col-md-6 order-1">
			<img src={loginImg} className='img-fluid shadow-4 h-75' alt='...' />
		</Col>
		</Row>
		</>

	)
}
