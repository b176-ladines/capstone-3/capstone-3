import React, {useState, useEffect} from 'react'

import {Table, Container, Card, Button, Row, Col} from 'react-bootstrap'

import {Link, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Cart(){
	
	const history = useNavigate();
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('')
	const [cart, setCart] = useState([])


	function cancelProduct (productId) {

	  	fetch(`http://localhost:4000/users/${productId}/cancel`,{

		method: 'DELETE',
		headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		    Swal.fire({
			  title: "Item Removed Successfully",
			  confirmButtonColor: '#38ba7c',
			  icon: 'success',
		    })
		    .then(okay => {
				if (okay) {
					window.location.reload();
				}
			})
		    
	}

	function checkOut (productId, status) {

	  	fetch(`http://localhost:4000/users/${productId}/checkOut`,{

		method: 'PUT',
		headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
					status: status
				})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		    Swal.fire({
			  title: "Successfully CheckOut",
			  confirmButtonColor: '#38ba7c',
			  icon: 'success',
		    }
		  )
		history("/")
		    
	}

	useEffect(() => {
			let token = localStorage.getItem('token');
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)

				let orderList = result.cart.map((product) => {
					console.log(product);
				return (
			    <tr>
			      <td>{product.name}</td>
			      <td>{product.status}</td>
			      <td>{product.purchasedOn}</td>
			      <td>{product.price}</td>
			      <td className="text-center"><Button
			      			className="mx-2"
							variant="dark"
							size="sm"
							onClick={() => cancelProduct(product.productId)}
						>
							Cancel
						</Button>
						<Button 
							className="mx-2"
							variant="warning" 
							size="sm" 
							onClick={() => checkOut(product.productId, product.status)}
						>
							Checkout
						</Button>
				</td>
			    </tr>
					)
				})

				setFirstName(result.firstName);
				setLastName(result.lastName);
				setEmail(result.email);
				setCart(orderList);

			})
		}, [])



	return(
		<Container fluid className=" text-white cartTable">
			<Row className="justify-content-center">
			<Card className="col-12 col-md-6 p-2 text-white">
				<Card.Body>
				<Row>
					<Col>
						<h5>First Name</h5>
					</Col>
					<Col>
					<h6 className="text-dark">{firstName}</h6>
					</Col>
				</Row>
				<Row>
					<Col>
					<h5>Last Name</h5>
					</Col>
					<Col>
					<h6 className="text-dark">{lastName}</h6>
					</Col>
				</Row>
				<Row>
					<Col>
					<h5>Email</h5>
					</Col>
					<Col>
					<h6 className="text-dark">{email}</h6>
					</Col>
				</Row>
				</Card.Body>
			</Card>
			</Row>

			<h3>Orders</h3>
			<Table className="text-white">
			  <tbody>
			  	<tr>
			      <th>Product Name</th>
				  <th>Product Status</th>
			      <th>Product Purchased Date</th>
			      <th>Product Price</th>
			      <th className="text-center">Actions</th>
			    </tr>
				{cart}
			</tbody>
			</Table>
		</Container>
	
		)
}