import React, {useState, useEffect, useContext} from 'react';
import {Container, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom';

import AdminView from './../components/AdminView.js';



import UserContext from './../UserContext';

export default function Admin(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {

		fetch('http://localhost:4000/products/all')
		.then(result => result.json())
		.then(data => {
			console.log(data)
			setProducts(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		
		<AdminView productData={products} fetchData={fetchData}/>
	)
}
