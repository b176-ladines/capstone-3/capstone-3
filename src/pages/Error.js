import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Error(){
	
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>404 Error: Page Not Found</h1>
				<p>This page doesn't exist</p>

				<Button as={Link} to="/" variant="primary">Back to Home</Button>
			</Col>
		</Row>
	)
}