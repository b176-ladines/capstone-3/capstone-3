import {Fragment} from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import img2 from '../images/img2.png'

export default function Home(){

	return(
		<Fragment>
			
			<div className="circle"></div>
			<Row>
				<Col className="col-12 col-md-7 mt-5 p-5 order-2 order-md-1">
					<h1 className="text-white mt-5">Welcome to AnimeTaku</h1>
					<p className="text-white">Are you an Anime Fan or an Otaku?</p>
					<p className="text-white">This shop aims to provide you with a wide range of figures and merchandise for your favorite anime.</p> 
					<Button className="btn-home" as={Link} to="/products">Shop Now!</Button>
				</Col>
				<Col className="col-12 col-md-5 order-1 order-md-2">
					
					<img src={img2} className='img-fluid' alt='...' />
				</Col>
			</Row>
			
			<div className="page-content">
				<div className="cardP">
					<div className="content">
						<h2 className="title">Demon Slayer</h2>
						<Button className="btn-card" as={Link} to="/products">See More</Button>
					</div>
				</div>
				<div className="cardP">
					<div className="content">
						<h2 className="title">One Piece</h2>
						<Button className="btn-card" as={Link} to="/products">See More</Button>
					</div>
				</div>
				<div className="cardP">
					<div className="content">
						<h2 className="title">Jujutsu Kaisen</h2>
						<Button className="btn-card" as={Link} to="/products">See More</Button>
					</div>
				</div>
			</div>
		</Fragment>
	)
}