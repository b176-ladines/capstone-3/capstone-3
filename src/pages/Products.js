import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, [])


	return(
		<>
		<h1 className="text-dark text-center pt-2">Products</h1>
		<h3 className="text-dark">Demon Slayer: </h3>
		<Container className="d-flex flex-wrap justify-content-center">
			{products}
		</Container>
		<h3 className="text-dark">One Piece: </h3>

		</>
	)
}

